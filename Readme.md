#Email collector

Implement a program in Java that crawls the web and collects e-mail
addresses from the pages it visits.

• The program reads the address of the first webpage from the
command line parameter.

• The program does not visit web pages twice, an exception being the
frst address which is given as the parameter.

• While running, the program prints/displays its current state, i.e
prints the address of the web page that is currently being processed
and notifes when an email is found.

• The program persists the email addresses it has found (any method
is okay).

• On each start, the program prints out/displays the e-mail addresses
that have been persisted.

You are free to choose any tools/libraries you like. Just a hint: Jsoup and
H2 database might become handy.
Dependencies must be managed with Maven or Gradle.



##Explanation of the Solution
I am using spring data for persistence, Jsoup to fetch content from the web,
h2 as a database in the file mode (notice the emaildb.mv.db file) and lombok to make less verbose the java code.

In general, the core of the solution is found in the class MainController, which consist of 3 steps
1. Print current state of DB
2. Validate arguments
3. Crawling the url for emails

I also handle some exceptions that are described in the Enum ErrorCode.java.

All methods contain a brief description.



##How do you start the application?

Execute in console or bash, inside the folder

`gradle assemble`

Then, in folder build/libs you will find the file email-collector-1.0.jar


For executing the app, remember to put an argument like http://www.google.com


`java -jar email-collector-1.0.jar http://www.google.com 
`

NOTE: This app only works under JAVA 8

##Some URLs for testing

http://www.google.com

https://www.avalanchelabs.ee

http://www.sapzurrocolombia.com

https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript?rq=1