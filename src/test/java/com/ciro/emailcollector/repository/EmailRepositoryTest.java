package com.ciro.emailcollector.repository;

import com.ciro.emailcollector.model.Email;
import com.ciro.emailcollector.model.Webpage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Jpa test for Email repository to make sure connection to database works
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class EmailRepositoryTest {

    @Autowired
    EmailRepository emailRepository;
    @Autowired
    WebpageRepository webpageRepository;

    private Webpage webpage;

    @Before
    public void setUp() throws Exception {
        webpage = webpageRepository.save(Webpage.builder().webpage("Test").createdDate(LocalDate.now()).build());
    }

    @Test
    public void saveEmail() {
        List<Email> emails = new ArrayList<>();

        emails.add(Email.builder().webpage(webpage).email("adrian@avengers.com").build());
        emails.add(Email.builder().webpage(webpage).email("thanos@avengers.com").build());
        emails.add(Email.builder().webpage(webpage).email("tony@avengers.com").build());
        emails.forEach(email -> emailRepository.save(email));

        assertTrue(emailRepository.findAll().size()==3);
    }


}