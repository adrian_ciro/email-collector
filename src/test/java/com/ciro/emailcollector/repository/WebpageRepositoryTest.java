package com.ciro.emailcollector.repository;

import com.ciro.emailcollector.model.Webpage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertTrue;

/**
 * Jpa test for Webpage repository to make sure connection to database works
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class WebpageRepositoryTest {

    @Autowired
    WebpageRepository webpageRepository;


    @Test
    public void saveWebpage() {
        webpageRepository.save(Webpage.builder().webpage("Test").createdDate(LocalDate.now()).build());
        assertTrue(webpageRepository.findAll().size()== 1);
    }
}