package com.ciro.emailcollector.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ValidationServiceTest {

    @InjectMocks
    private ValidationService validationService;
    @Mock
    private PersistenceService persistenceService;


    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void isValidArgument() {
        String[] args = {"http://www.google.com"};

        when(persistenceService.isUrlAlreadyChecked(anyString())).thenReturn(false);

        boolean isValid=validationService.isValidArgument(args);

        assertTrue(isValid);

    }

    @Test
    public void isNotValidArgument() {
        String[] args = {"http://www.google.com"};

        when(persistenceService.isUrlAlreadyChecked(anyString())).thenReturn(true);

        boolean isValid=validationService.isValidArgument(args);

        assertFalse(isValid);

    }

    @Test
    public void isNotValidBecauseSeveralArguments() {

        String[] args = {"http://www.google.com", "Other url"};

        boolean isValid=validationService.isValidArgument(args);

        assertFalse(isValid);
    }
}