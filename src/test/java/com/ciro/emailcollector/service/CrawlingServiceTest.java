package com.ciro.emailcollector.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class CrawlingServiceTest {

    private CrawlingService crawlingService;

    @Before
    public void setUp() throws Exception {
        crawlingService = new CrawlingService();
    }

    @Test
    public void findEmailsInContent() {

        String content = "sairamkrishna@tutorialspoint.com : true\n" +
                "kittuprasad700@gmail.com : true\n" +
                "sairamkrishna_mammahe%google-india.com : false\n" +
                "sairam.krishna@gmail-indai.com : true\n" +
                "sai#@youtube.co.in : true\n" +
                "kittu@domaincom : false\n" +
                "kittu#gmail.com : false\n" +
                "@pindom.com : false";

        List<String> emails = crawlingService.findEmailsFromContent(content);

        assertTrue(emails.size()==3);
    }

    @Test
    public void findEmailsInAnEmptyContent() {
        List<String> emails = crawlingService.findEmailsFromContent("");
        assertTrue(emails.isEmpty());
    }
}