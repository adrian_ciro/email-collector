package com.ciro.emailcollector;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { Application.class })
public class ApplicationITest {

	/**
	 * My aim with this empty test is to be sure that entire application is runnable. Makes no sense to give
	 * arguments like the url to start the process if there is no connection to internet. In that case test will fail.
	 * Nonetheless, the other features are covered in the service tests.
	 */
	@Test
	public void contextLoads() {
	}

}
