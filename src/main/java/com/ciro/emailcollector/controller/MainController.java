package com.ciro.emailcollector.controller;

import com.ciro.emailcollector.service.CrawlingService;
import com.ciro.emailcollector.service.PersistenceService;
import com.ciro.emailcollector.service.ValidationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Main controller responsible for initialize services and execute the steps required for printing the current status of database,
 * validate the url argument and later find the emails in the html page
 * Created by Adrian
 */
@Component
@AllArgsConstructor
public class MainController {

    private ValidationService validationService;
    private CrawlingService crawlingService;
    private PersistenceService persistenceService;


    /**
     * This is the core of the application dived in 3 steps
     * 1. Print current state of DB
     * 2. Validate arguments
     * 3. Crawling the url for emails
     * @param args
     */
    public void start(String[] args) {
        //Step 1
        persistenceService.printCurrentWebpages();

        //Step 2
        if (validationService.isValidArgument(args)) {
            //Step 3
            startCrawlingProcess(args[0]);
        }
    }

    /**
     * Method for fetching the content from the web and the emails
     * using the crawling service. Either emails are found or not, the system will persist the webpage.
     * @param url
     */
    public void startCrawlingProcess(String url) {
        String content = crawlingService.fetchContentFromUrl(url);
        List<String> foundEmails = crawlingService.findEmailsFromContent(content);
        persistenceService.saveEmails(url, foundEmails);
    }


}
