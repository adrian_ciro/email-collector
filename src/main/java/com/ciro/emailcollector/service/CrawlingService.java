package com.ciro.emailcollector.service;

import com.ciro.emailcollector.model.ErrorCode;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Service for crawling a specific url and find out if it contains emails
 * Created by Adrian
 */
@Service
public class CrawlingService {

    public static final int TIME_OUT_LIMIT = 10000;
    public static String EMAIL_REGEX = "([_A-Za-z0-9-]+)(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,4})";
    public static String STARTING_PROCESS_MSG = "Starting process of finding emails on page ";
    public static String EMAILS_NO_FOUND_MSG = "Emails not found in the current process ";
    public static String EMAILS_FOUND_MSG = "Number of emails found in total: ";


    /**
     * Get content from the web using Jsoup
     * @param url Required url in format http://...
     * @return Clean html from the web url
     */
    public String fetchContentFromUrl(String url) {
        try {
            System.out.println(STARTING_PROCESS_MSG + url);
            Document doc = Jsoup.connect(url).timeout(TIME_OUT_LIMIT).get();
            return Jsoup.clean(doc.html(), Whitelist.none());
        } catch (IOException e) {
            System.out.println(ErrorCode.ERROR_103.getErrorMessage() + e.getMessage());
            return null;
        }
    }

    /**
     * Using an email regex, find if there are emails for the given url
     * @param content String to be analyzed
     * @return A list of emails. Empty list if there is none
     */
    public List<String> findEmailsFromContent(String content) {

        if (StringUtils.isEmpty(content)) {
            return Collections.emptyList();
        }

        List<String> emails = new ArrayList<>();
        Pattern pattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(content);

        while (matcher.find()) {
            emails.add(matcher.group());
            System.out.println("Email found: " + matcher.group());
        }

        if(emails.isEmpty()){
            System.out.println(EMAILS_NO_FOUND_MSG);
            return Collections.emptyList();
        }

        System.out.println(EMAILS_FOUND_MSG + emails.size());
        return emails;
    }
}
