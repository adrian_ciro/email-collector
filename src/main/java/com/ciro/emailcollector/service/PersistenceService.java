package com.ciro.emailcollector.service;

import com.ciro.emailcollector.model.Email;
import com.ciro.emailcollector.model.Webpage;
import com.ciro.emailcollector.repository.EmailRepository;
import com.ciro.emailcollector.repository.WebpageRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Service for persisting email information in database and make specific queries
 */
@Service
@AllArgsConstructor
public class PersistenceService {

    private WebpageRepository webpageRepository;
    private EmailRepository emailRepository;

    /**
     * Save info related to the url scanned and the emails found
     * @param url
     * @param emails
     */
    public void saveEmails(String url, List<String> emails){

        Webpage webpage = Webpage.builder().webpage(url).createdDate(LocalDate.now()).build();

        List<Email> emailList = new ArrayList<>();
        emails.forEach(email -> emailList.add(Email.builder().email(email).webpage(webpage).build()));

        webpage.setEmails(emailList);
        webpageRepository.save(webpage);
        emailList.forEach( email -> emailRepository.save(email));
    }

    /**
     * Query in DB if the given url is in the webpage records
     * @param url
     * @return True if url is found. False otherwise
     */
    public boolean isUrlAlreadyChecked(String url){
        Example<Webpage> webpageExample = Example.of(Webpage.builder().webpage(url).build());
        return webpageRepository.findOne(webpageExample).isPresent();
    }

    /**
     * Find all the records for webpages and print the linked emails
     */
    public void printCurrentWebpages(){
        List<Webpage> webpages = webpageRepository.findAll();

        if(webpages.isEmpty()){
            System.out.println("The current status of database is empty. No emails have been persisted");
            return;
        }

        System.out.println("Printing current status of database");
        webpages.forEach(web -> printEmails(web.getWebpage(), web.getEmails()));
        System.out.println("");
    }

    /**
     * Print the specific emails of the given url
     * @param url
     * @param emails
     */
    public void printEmails(String url, List<Email> emails){
        System.out.println("*********************");
        System.out.println("Current list of emails for " + url);
        emails.forEach(email -> System.out.println(email.getEmail()));
        System.out.println("*********************");
    }
}
