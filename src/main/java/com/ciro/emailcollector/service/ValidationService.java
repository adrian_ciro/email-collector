package com.ciro.emailcollector.service;

import com.ciro.emailcollector.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Service for validating format and length of the argument. Also verify if the given argument is already in DB
 */
@Service
public class ValidationService {

    final static String HTTP_PREFIX = "http://";
    final static String HTTPS_PREFIX = "https://";

    @Autowired
    private PersistenceService persistenceService;

    /**
     * Validate if the given array only has one argument
     * @param args
     * @return True if there is only one argument in args and this one is valid. False otherwise
     */
    public boolean isValidArgument(String[] args) {

        if(args.length == 1){
           return isUrlFormattedAndNotCheckedBefore(args[0]);
        }

        System.out.println(ErrorCode.ERROR_100.getErrorMessage());
        return false;
    }

    /**
     * Validate that the given url has the format http:// or https://
     * Also verify that the url is not in DB
     * @param url
     * @return True if url is valid and it is not in DB. False otherwise.
     */
    public boolean isUrlFormattedAndNotCheckedBefore(String url) {

        if(StringUtils.isEmpty(url) || !(url.contains(HTTP_PREFIX) || url.contains(HTTPS_PREFIX))){
            System.out.println(ErrorCode.ERROR_101.getErrorMessage());
            return false;
        }

        if(persistenceService.isUrlAlreadyChecked(url)){
            System.out.println(ErrorCode.ERROR_102.getErrorMessage());
            return false;
        }

        return true;
    }
}
