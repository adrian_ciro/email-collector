package com.ciro.emailcollector.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Enum to define all the errors that can be present in the application
 * Created by Adrian
 */
@Getter
@AllArgsConstructor
public enum ErrorCode {

    ERROR_100("100", "Error in the application. Parameters are invalid for web crawling. Be sure it has only one argument"),
    ERROR_101("101", "Error in the application. URL is malformed. Please correct the URL in the format http://***"),
    ERROR_102("102", "The url given was already processed"),
    ERROR_103("103", "Error with the connection or timeOut reached for the url. ");

    private final String errorCode;
    private final String errorMessage;

}
