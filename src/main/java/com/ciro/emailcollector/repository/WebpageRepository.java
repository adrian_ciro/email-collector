package com.ciro.emailcollector.repository;

import com.ciro.emailcollector.model.Webpage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WebpageRepository extends JpaRepository<Webpage, Long> {
}
