package com.ciro.emailcollector.repository;

import com.ciro.emailcollector.model.Email;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRepository extends JpaRepository<Email, Long> {
}
